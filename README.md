# TIL

### APIs

- If you're using a remote service, make sure that only a particular endpoint returns a response from that service. For example, if have a "Record" instance and for that instance you're using a Status Service, rather than doing:

```
GET /api/v1/record/{}

Responds:
{
    "field1": "",
    "field2": "",
    "status": "SOMETHING"
}
```

Do the following:

```
GET /api/v1/record/{}/status

Responds:
{
    "status": "SOMETHING",
}
```

### Python

- If you have a task in celery that pulls some remote resource, the first thing comes to mind is to create loop:

```python
@celery_app.task()
def pull_remote_resource(self, _id):
    while True:
        response = pull(_id)
        if not response:
            time.sleep(4)
```

But this doesn't solve the problem, it simply moves blocking call from web server workers to celery worker. After ~20 tasks, your all workers will be exhausted as well. But you can do the following:

```python
@celery_app.task(bind=True, max_retries=10)
def pull_remote_resource(self, _id):
    response = pull(_id)
    if not response:
        self.retry(countdown=3)
```

You can even provide dynamic countdown timer:

```python
@celery_app.task(bind=True, max_retries=10)
def pull_remote_resource(self, _id):
    response = pull(_id)
    if response:
        pass # do actual thing here

    # in initial retries, check every 2 seconds
    if self.request.retries < 5:
        countdown = 2
    # after 10 seconds, check every 5 seconds
    elif self.request.retries < 15:
        countdown = 5
    # after 60 seconds check every 10 seconds
    else:
        countdown = 10
    self.retry(countdown=countdown)
```

### Design

#### Figma

- If you want to use auto-layout with padding & borders in figma, instead of adding a padding to auto-layout, wrap it in another auto-layout and add padding in there.
This way, you can scale your bordered content both from inside (scales if internal content grows) and outside (border scales if encapsulating frame grows).

### Work

#### Good Manager

Google made a research and found following traits important for the managers (https://rework.withgoogle.com/blog/the-evolution-of-project-oxygen/):

- Is a good coach
- Empowers team and does not micromanage
- Creates an inclusive team environment, showing concern for success and well-being
- Is productive and results-oriented
- Is a good communicator — listens and shares information
- Supports career development and discusses performance
- Has a clear vision/strategy for the team
- Has key technical skills to help advise the team
- Collaborates across Google
- Is a strong decision maker

#### About Attitude

There always are bits that you could do better. Always aspire for 120% of yourself, don't be satisfied with anything less than 100%. But if you've given your 100%, you should know that your new limit is higher than the previous one.
